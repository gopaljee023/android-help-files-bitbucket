//layout/custom_notification.xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:id="@+id/toast_layout_root"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:padding="3dp" >

    <ImageView
        android:id="@+id/image"
        android:layout_width="44dp"
        android:layout_height="44dp"
        android:layout_marginRight="10dp"
        android:contentDescription="@string/eye_desc_string"
        android:src="@drawable/fire_eye_alien" />

    <TextView
        android:id="@+id/text"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:textColor="#FFF"
        android:textSize="24sp" />

</LinearLayout>


//
RemoteViews mContentView = new RemoteViews(
		"course.examples.notification.statusbarwithcustomview",
		R.layout.custom_notification);


/*******************From NatificationStatusBar.txt************/

// Notification ID to allow for future updates
private static final int MY_NOTIFICATION_ID = 1;

// Notification Count
private int mNotificationCount;

// Notification Text Elements
private final CharSequence tickerText = "This is a Really, Really, Super Long Notification Message!";
private final CharSequence contentText = "You've Been Notified!";

// Notification Action Elements
private Intent mNotificationIntent;
private PendingIntent mContentIntent;
// Define the Notification's expanded message and Intent:

		mContentView.setTextViewText(R.id.text, contentText + " ("
		+ ++mNotificationCount + ")");

		// Build the Notification

		Notification.Builder notificationBuilder = new Notification.Builder(
		getApplicationContext())
		.setTicker(tickerText)
		.setSmallIcon(android.R.drawable.stat_sys_warning)
		.setAutoCancel(true)
		.setContentIntent(mContentIntent)
		.setSound(soundURI)
		.setVibrate(mVibratePattern)
		.setContent(mContentView);

		// Pass the Notification to the NotificationManager:
		NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.notify(MY_NOTIFICATION_ID,
		notificationBuilder.build());