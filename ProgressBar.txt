<ProgressBar
        android:id="@+id/progressBar"
        style="@android:style/Widget.ProgressBar.Horizontal"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:maxHeight="5dip"
        android:minHeight="5dip"
        android:visibility="invisible" >
  </ProgressBar>



//usage
ProgressBar  mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
mProgressBar.setVisibility(ProgressBar.VISIBLE);
for (int i = 1; i < 11; i++) {
								
int step = i;
mProgressBar.setProgress(step * 10);
			
}

mProgressBar.setVisibility(ProgressBar.INVISIBLE);