==============================================
//floating  lable to display hints and error messages
<android.support.design.widget.TextInputLayout
    android:id="@+id/username_text_input_layout"
    android:layout_width="match_parent"
    android:layout_height="wrap_content">

    <EditText
        android:id="@+id/etUsername"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_centerHorizontal="true"
        android:layout_centerVertical="true"
        android:ems="10"
        android:hint="Username" />

</android.support.design.widget.TextInputLayout>


=====================================
//how to show error message using TextInputLayout

private void setupFloatingLabelError() {
    final TextInputLayout floatingUsernameLabel = (TextInputLayout) findViewById(R.id.username_text_input_layout);
    floatingUsernameLabel.getEditText().addTextChangedListener(new TextWatcher() {
        // ...
        @Override
        public void onTextChanged(CharSequence text, int start, int count, int after) {
            if (text.length() > 0 && text.length() <= 4) {
                floatingUsernameLabel.setError(getString(R.string.username_required));
                floatingUsernameLabel.setErrorEnabled(true);
            } else {
                floatingUsernameLabel.setErrorEnabled(false);
            }
        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, 
                                      int after) {
            // TODO Auto-generated method stub
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    });
}


===================================================
//Adding Character Counting
The TextInputLayout simply needs to define app:counterEnabled and app:CounterMaxLength in the XML attributes. 

e.g.
<android.support.design.widget.TextInputLayout
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    app:counterEnabled="true" <<<<<<<<<<<<<<<-check this
    app:counterMaxLength="10" <<<<<<<<<<<<<<<-check this
    app:counterTextAppearance="@style/counterText”  <<<<<<<<<<<<<<<-check this
    app:counterOverflowTextAppearance="@style/counterOverride”>. <<<<<<<<<<<<<<<-check this
    <EditText
       android:layout_width="wrap_content"
       android:layout_height="wrap_content"
       android:hint="Username"
       android:layout_centerHorizontal="true"
       android:layout_centerVertical="true"
       android:ems="10"
       android:hint="Username" />
</android.support.design.widget.TextInputLayout>


Add following style
<style name="counterText">
  <item name="android:textColor">#aa5353cc</item>
</style>

<style name="counterOverride">
  <item name="android:textColor">#ff0000</item>
</style>


===========================================================

//Adding Password Visibility Toggles
NOTE: You must have support library 24.2.0 or higher to use this feature.


<android.support.design.widget.TextInputLayout
        android:id="@+id/username_text_input_layout"
        app:passwordToggleEnabled="true”.  <<<<<<<<<<<<<<<-check this
        android:layout_width="match_parent"
        android:layout_height="wrap_content">

        <EditText
            android:id="@+id/etUsername"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_centerHorizontal="true"
            android:layout_centerVertical="true"
            android:ems="10"
            android:inputType="textPassword"
            android:hint="Username" />

    </android.support.design.widget.TextInputLayout>