//1.main.xml : ImageView, and Bottom button , which will start the animation

<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:paddingBottom="@dimen/activity_vertical_margin"
    android:paddingLeft="@dimen/activity_horizontal_margin"
    android:paddingRight="@dimen/activity_horizontal_margin"
    android:paddingTop="@dimen/activity_vertical_margin"
    tools:context=".ValueAnimatorActivity" >

    <ImageView
        android:id="@+id/image_view"
        android:layout_width="match_parent"
        android:layout_height="400dp"
        android:contentDescription="@string/app_name" />

    <Button
        android:id="@+id/start_animation_button"
        android:layout_width="match_parent"
        android:layout_height="48dp"
        android:layout_alignParentBottom="true"
        android:text="@string/button_label"/>

</RelativeLayout>


//2 activity class

public class ValueAnimatorActivity extends Activity {

	protected static final String TAG = "ValueAnimatorActivity";
	final private static int RED = Color.RED;
	final private static int BLUE = Color.BLUE;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		Button startButton = (Button) findViewById(R.id.start_animation_button);
		startButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startAnimation();
			}
		});
		
	}

	public void startAnimation() {
		
		final ImageView imageView = (ImageView) findViewById(R.id.image_view);
		
		ValueAnimator anim = ValueAnimator.ofObject(new ArgbEvaluator(), RED,
				BLUE);  //JEE1

		anim.addUpdateListener(new AnimatorUpdateListener() {

			@Override
			public void onAnimationUpdate(ValueAnimator animation) {
				imageView.setBackgroundColor((Integer) animation
						.getAnimatedValue());
			}
		});
		
		anim.setDuration(10000);
		anim.start();
	}
	
}