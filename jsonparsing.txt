private static final String LONGITUDE_TAG = "lng";
	private static final String LATITUDE_TAG = "lat";
	private static final String MAGNITUDE_TAG = "magnitude";
	private static final String EARTHQUAKE_TAG = "earthquakes";
	public List<String> handleResponse(String jsonResponse) {
		List<String> result = new ArrayList<String>();
		String JSONResponse = jsonResponse;
		try {

			// Get top-level JSON Object - a Map
			JSONObject responseObject = (JSONObject) new JSONTokener(
					JSONResponse).nextValue();

			// Extract value of "earthquakes" key -- a List
			JSONArray earthquakes = responseObject
					.getJSONArray(EARTHQUAKE_TAG);

			// Iterate over earthquakes list
			for (int idx = 0; idx < earthquakes.length(); idx++) {

				// Get single earthquake data - a Map
				JSONObject earthquake = (JSONObject) earthquakes.get(idx);

				// Summarize earthquake data as a string and add it to
				// result
				result.add(MAGNITUDE_TAG + ":"
						+ earthquake.get(MAGNITUDE_TAG) + ","
						+ LATITUDE_TAG + ":"
						+ earthquake.getString(LATITUDE_TAG) + ","
						+ LONGITUDE_TAG + ":"
						+ earthquake.get(LONGITUDE_TAG));
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return result;
	}

String JSONResponse  = "{\n" +
			"  \"earthquakes\": [\n" +
			"    {\n" +
			"      \"datetime\": \"2011-03-11 04:46:23\",\n" +
			"      \"depth\": 24.4,\n" +
			"      \"lng\": 142.369,\n" +
			"      \"src\": \"us\",\n" +
			"      \"eqid\": \"c0001xgp\",\n" +
			"      \"magnitude\": 8.8,\n" +
			"      \"lat\": 38.322\n" +
			"    },\n" +
			"    {\n" +
			"      \"datetime\": \"2012-04-11 06:38:37\",\n" +
			"      \"depth\": 22.9,\n" +
			"      \"lng\": 93.0632,\n" +
			"      \"src\": \"us\",\n" +
			"      \"eqid\": \"c000905e\",\n" +
			"      \"magnitude\": 8.6,\n" +
			"      \"lat\": 2.311\n" +
			"    }\n" +
			"  ]\n" +
			"}";









///usage

	/*
	{
  "earthquakes": [
    {
      "datetime": "2011-03-11 04:46:23",
      "depth": 24.4,
      "lng": 142.369,
      "src": "us",
      "eqid": "c0001xgp",
      "magnitude": 8.8,
      "lat": 38.322
    },
    {
      "datetime": "2012-04-11 06:38:37",
      "depth": 22.9,
      "lng": 93.0632,
      "src": "us",
      "eqid": "c000905e",
      "magnitude": 8.6,
      "lat": 2.311
    }
  ]
}
	 */
	String JSONResponse  = "{\n" +
			"  \"earthquakes\": [\n" +
			"    {\n" +
			"      \"datetime\": \"2011-03-11 04:46:23\",\n" +
			"      \"depth\": 24.4,\n" +
			"      \"lng\": 142.369,\n" +
			"      \"src\": \"us\",\n" +
			"      \"eqid\": \"c0001xgp\",\n" +
			"      \"magnitude\": 8.8,\n" +
			"      \"lat\": 38.322\n" +
			"    },\n" +
			"    {\n" +
			"      \"datetime\": \"2012-04-11 06:38:37\",\n" +
			"      \"depth\": 22.9,\n" +
			"      \"lng\": 93.0632,\n" +
			"      \"src\": \"us\",\n" +
			"      \"eqid\": \"c000905e\",\n" +
			"      \"magnitude\": 8.6,\n" +
			"      \"lat\": 2.311\n" +
			"    }\n" +
			"  ]\n" +
			"}";



List<String> result = handleResponse(JSONResponse);