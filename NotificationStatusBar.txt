// Notification ID to allow for future updates
private static final int MY_NOTIFICATION_ID = 1;
// Notification Text Elements
private final CharSequence tickerText = "Only 3% people write diaries. You are among them!!!";
private final CharSequence contentTitle = "Diary writing Notification";
private final CharSequence contentText = "Please write your evernote journal!";


// Notification Action Elements
private Intent mNotificationIntent;
private PendingIntent mContentIntent;

// Notification Sound and Vibration on Arrival  
private Uri soundURI = Uri
		.parse("android.resource://course.examples.notification.statusbar/"
		+ R.raw.alarm_rooster);
private long[] mVibratePattern = { 0, 200, 200, 300 };



//create instant
mNotificationIntent = new Intent(getApplicationContext(),
				NotificationSubActivity.class);
mContentIntent = PendingIntent.getActivity(getApplicationContext(), 0,
				mNotificationIntent, Intent.FLAG_ACTIVITY_NEW_TASK);



// Define the Notification's expanded message and Intent:

	Notification.Builder notificationBuilder = new Notification.Builder(
			getApplicationContext())
			.setTicker(tickerText)
			.setSmallIcon(android.R.drawable.stat_sys_warning)
			.setAutoCancel(true)
			.setContentTitle(contentTitle)
			.setContentText(
			contentText + " (" + ++mNotificationCount + ")")
			.setContentIntent(mContentIntent).setSound(soundURI)
			.setVibrate(mVibratePattern);

			// Pass the Notification to the NotificationManager:
			NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
			mNotificationManager.notify(MY_NOTIFICATION_ID,
			notificationBuilder.build());
