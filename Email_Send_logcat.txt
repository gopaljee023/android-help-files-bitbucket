    public static void sendLog(Context context){
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, "logs");
        intent.putExtra(Intent.EXTRA_TEXT, Utils.getLog());
        Intent mailer = Intent.createChooser(intent, null);
        context.startActivity(mailer);
    }



    public static String getLog(){
        StringBuilder log = new StringBuilder();

        try {
            Process process = Runtime.getRuntime().exec("logcat -d");
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(process.getInputStream()));

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                log.append(line+"\n");
            }

        } catch (IOException e) {
        }
        return log.toString();
    }
