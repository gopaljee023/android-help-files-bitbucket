//Compound broadcast

//Manifest file

 <uses-permission android:name="android.permission.VIBRATE" />
 <receiver
            android:name=".Receiver2"
            android:exported="false" >
            <intent-filter>
                <action android:name="course.examples.BroadcastReceiver.show_toast" >
                </action>
            </intent-filter>
        </receiver>
        <receiver
            android:name=".Receiver3"
            android:exported="false" >
            <intent-filter>
                <action android:name="course.examples.BroadcastReceiver.show_toast" >
                </action>
            </intent-filter>
        </receiver>



//Receiver1
public class Receiver1 extends BroadcastReceiver {
	private final String TAG = "Receiver1";

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.i(TAG, "INTENT RECEIVED");

		Vibrator v = (Vibrator) context
				.getSystemService(Context.VIBRATOR_SERVICE);
		v.vibrate(500);

		Toast.makeText(context, "INTENT RECEIVED by Receiver1", Toast.LENGTH_LONG).show();
	}

}


//Receiver2
public class Receiver2 extends BroadcastReceiver {
	private final String TAG = "Receiver2";

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.i(TAG, "INTENT RECEIVED");

		Vibrator v = (Vibrator) context
				.getSystemService(Context.VIBRATOR_SERVICE);
		v.vibrate(500);

		Toast.makeText(context, "INTENT RECEIVED by Receiver2", Toast.LENGTH_LONG).show();
	}

}


//receiver3
public class Receiver3 extends BroadcastReceiver {
	private final String TAG = "Receiver3";

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.i(TAG, "INTENT RECEIVED");

		Vibrator v = (Vibrator) context
				.getSystemService(Context.VIBRATOR_SERVICE);
		v.vibrate(500);

		Toast.makeText(context, "INTENT RECEIVED by Receiver3", Toast.LENGTH_LONG).show();
	}

}

//from activity
private static final String CUSTOM_INTENT = "course.examples.BroadcastReceiver.show_toast";
	private final Receiver1 receiver1 = new Receiver1();
	private final IntentFilter intentFilter = new IntentFilter(CUSTOM_INTENT);


//register your receiver1
registerReceiver(receiver1, intentFilter);

sendBroadcast(new Intent(CUSTOM_INTENT),android.Manifest.permission.VIBRATE);

unregisterReceiver(receiver1);