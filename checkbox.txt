	
1.java file
// Get a reference to the CheckBox
		final CheckBox checkbox = (CheckBox) findViewById(R.id.checkbox);

		// Set an OnClickListener on the CheckBox
		checkbox.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				// Check whether CheckBox is currently checked
				// Set CheckBox text accordingly
				if (checkbox.isChecked()) {
					checkbox.setText("I'm checked");
				} else {
					checkbox.setText("I'm not checked");
				}
			}
		});



Xml file

    <CheckBox
        android:id="@+id/checkbox"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="@string/im_not_checked_string" 
        android:textSize="24sp"/>