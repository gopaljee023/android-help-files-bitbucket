//Part of AlarmCreateActivity.java
private AlarmManager mAlarmManager;
// Get the AlarmManager Service
mAlarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
private Intent mNotificationReceiverIntent, mLoggerReceiverIntent;
private PendingIntent mNotificationReceiverPendingIntent,
		mLoggerReceiverPendingIntent;

// Create an Intent to broadcast to the AlarmNotificationReceiver
		mNotificationReceiverIntent = new Intent(AlarmCreateActivity.this,
		AlarmNotificationReceiver.class);

		// Create an PendingIntent that holds the NotificationReceiverIntent
		mNotificationReceiverPendingIntent = PendingIntent.getBroadcast(
		AlarmCreateActivity.this, 0, mNotificationReceiverIntent, 0);

		// Create an Intent to broadcast to the AlarmLoggerReceiver
		mLoggerReceiverIntent = new Intent(AlarmCreateActivity.this,
		AlarmLoggerReceiver.class);

		// Create PendingIntent that holds the mLoggerReceiverPendingIntent
		mLoggerReceiverPendingIntent = PendingIntent.getBroadcast(
		AlarmCreateActivity.this, 0, mLoggerReceiverIntent, 0);

// Set single alarm
		mAlarmManager.set(AlarmManager.RTC_WAKEUP,
		System.currentTimeMillis() + INITIAL_ALARM_DELAY,
		mNotificationReceiverPendingIntent);

		// Set single alarm to fire shortly after previous alarm
		mAlarmManager.set(AlarmManager.RTC_WAKEUP,
		System.currentTimeMillis() + INITIAL_ALARM_DELAY
		+ JITTER, mLoggerReceiverPendingIntent);






/*********** AlarmLoggerReceiver ******/


public class AlarmLoggerReceiver extends BroadcastReceiver {

	private static final String TAG = "AlarmLoggerReceiver";
	@Override
	public void onReceive(Context context, Intent intent) {

		// Log receipt of the Intent with timestamp
		Log.i(TAG,"Logging alarm at:" + DateFormat.getDateTimeInstance().format(new Date()));

	}
}

/*********** AlarmNotificationReceiver**********/

public class AlarmNotificationReceiver extends BroadcastReceiver {
	// Notification ID to allow for future updates
	private static final int MY_NOTIFICATION_ID = 1;
	private static final String TAG = "AlarmNotificationReceiver";

	// Notification Text Elements
	private final CharSequence tickerText = "Are You Playing Angry Birds Again!";
	private final CharSequence contentTitle = "A Kind Reminder";
	private final CharSequence contentText = "Get back to studying!!";

	// Notification Action Elements
	private Intent mNotificationIntent;
	private PendingIntent mContentIntent;

	// Notification Sound and Vibration on Arrival
	private final Uri soundURI = Uri
			.parse("android.resource://course.examples.Alarms.AlarmCreate/"
					+ R.raw.alarm_rooster);
	private final long[] mVibratePattern = { 0, 200, 200, 300 };

	@Override
	public void onReceive(Context context, Intent intent) {

		// The Intent to be used when the user clicks on the Notification View
		mNotificationIntent = new Intent(context, AlarmCreateActivity.class);

		// The PendingIntent that wraps the underlying Intent
		mContentIntent = PendingIntent.getActivity(context, 0,
				mNotificationIntent, Intent.FLAG_ACTIVITY_NEW_TASK);

		// Build the Notification
		Notification.Builder notificationBuilder = new Notification.Builder(
				context).setTicker(tickerText)
				.setSmallIcon(android.R.drawable.stat_sys_warning)
				.setAutoCancel(true).setContentTitle(contentTitle)
				.setContentText(contentText).setContentIntent(mContentIntent)
				.setSound(soundURI).setVibrate(mVibratePattern);

		// Get the NotificationManager
		NotificationManager mNotificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);

		// Pass the Notification to the NotificationManager:
		mNotificationManager.notify(MY_NOTIFICATION_ID,
				notificationBuilder.build());

		// Log occurence of notify() call
		Log.i(TAG, "Sending notification at:"
				+ DateFormat.getDateTimeInstance().format(new Date()));

	}
}


/*******Manifestfile******/
<uses-permission android:name="android.permission.VIBRATE" />
   <receiver android:name=".AlarmNotificationReceiver" />
       

   <receiver android:name=".AlarmLoggerReceiver" />
      