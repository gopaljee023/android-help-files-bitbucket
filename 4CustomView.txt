//You should inherit from View if update is not frequent…if every 1 -2 seconds..otherwise subclass from surface view
//1.main.xml

<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:id="@+id/frame"
    android:layout_width="match_parent"
    android:layout_height="match_parent" >

    <FrameLayout
        android:id="@+id/image"
        android:layout_width="200dp"
        android:layout_height="200dp"
        android:contentDescription="@string/bubble_desc" />

</RelativeLayout>











2.//bubbleActivity.java

public class BubbleActivity extends Activity {
	protected static final String TAG = "BubbleActivity";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		final RelativeLayout frame = (RelativeLayout) findViewById(R.id.frame);
		final Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
				R.drawable.b128);
		final BubbleView bubbleView = new BubbleView(getApplicationContext(),
				bitmap);

		frame.addView(bubbleView);

		new Thread(new Runnable() {
			@Override
			public void run() {
				while (bubbleView.move()) {
					bubbleView.postInvalidate();
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						Log.i(TAG, "InterruptedException");
					}

				}
			}
		}).start();
	}

	private class BubbleView extends View {

		private static final int STEP = 100;
		final private Bitmap mBitmap;
		
		private Coords mCurrent;
		final private Coords mDxDy;

		final private DisplayMetrics mDisplayMetrics;
		final private int mDisplayWidth;
		final private int mDisplayHeight;
		final private int mBitmapWidthAndHeight, mBitmapWidthAndHeightAdj;
		final private Paint mPainter = new Paint();

		public BubbleView(Context context, Bitmap bitmap) {
			super(context);

			mBitmapWidthAndHeight = (int) getResources().getDimension(
					R.dimen.image_height);
			this.mBitmap = Bitmap.createScaledBitmap(bitmap,
					mBitmapWidthAndHeight, mBitmapWidthAndHeight, false);

			mBitmapWidthAndHeightAdj = mBitmapWidthAndHeight + 20;
			
			mDisplayMetrics = new DisplayMetrics();
			BubbleActivity.this.getWindowManager().getDefaultDisplay()
					.getMetrics(mDisplayMetrics);
			mDisplayWidth = mDisplayMetrics.widthPixels;
			mDisplayHeight = mDisplayMetrics.heightPixels;

			Random r = new Random();
			float x = (float) r.nextInt(mDisplayWidth);
			float y = (float) r.nextInt(mDisplayHeight);
			mCurrent = new Coords(x, y);

			float dy = (float) r.nextInt(mDisplayHeight) / mDisplayHeight;
			dy *= r.nextInt(2) == 1 ? STEP : -1 * STEP;
			float dx = (float) r.nextInt(mDisplayWidth) / mDisplayWidth;
			dx *= r.nextInt(2) == 1 ? STEP : -1 * STEP;
			mDxDy = new Coords(dx, dy);

			mPainter.setAntiAlias(true);

		}

		@Override
		protected void onDraw(Canvas canvas) {
			Coords tmp = mCurrent.getCoords();
			canvas.drawBitmap(mBitmap, tmp.mX, tmp.mY, mPainter);
		}

		protected boolean move() {
			mCurrent = mCurrent.move(mDxDy);

			if (mCurrent.mY < 0 - mBitmapWidthAndHeightAdj
					|| mCurrent.mY > mDisplayHeight + mBitmapWidthAndHeightAdj
					|| mCurrent.mX < 0 - mBitmapWidthAndHeightAdj
					|| mCurrent.mX > mDisplayWidth + mBitmapWidthAndHeightAdj) {
				return false;
			} else {
				return true;
			}
		}
	}
}



















//3: support class:Coords.java

public class Coords {
	float mX;
	float mY;

	public Coords(float x, float y) {
		mX = x;
		mY = y;
	}

	synchronized Coords move (Coords dxdy) {
		return new Coords(mX + dxdy.mX , mY + dxdy.mY);
	}
	
	synchronized Coords getCoords() {
		return new Coords(mX, mY);
	}
	 
	@Override
	public String toString () {
		return "(" + mX + "," + mY + ")";
	}
}


//4,
 <dimen name="image_height">200dp</dimen>

